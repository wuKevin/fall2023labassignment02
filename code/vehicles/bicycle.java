// Kevin Wu 2242923
package vehicles;

public class bicycle {

    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public String getManufacturer() {
        return this.manufacturer;
    }

    public int getNumberGears() {
        return this.numberGears;
    }

    public double getMaxSpeed() {
        return this.maxSpeed;
    }

    public bicycle(String manufacturer, int numberGears, double maxSpeed) {
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    public String toString() {
        String display = "";
        display = "Manufacturer: " + this.manufacturer + ", Number of Gears: " + this.numberGears + ", MaxSpeed: "
                + this.maxSpeed;
        return display;
    }

}