//Kevin Wu 2242923
package mainpackagelab2;

import vehicles.bicycle;

public class bikestore {
    public static void main(String[] args) {

        vehicles.bicycle[] bikes = new vehicles.bicycle[4];

        bikes[0] = new vehicles.bicycle("trek", 30, 60.0);
        bikes[1] = new vehicles.bicycle("kona", 12, 45.0);
        bikes[2] = new vehicles.bicycle("santa cruz", 18, 52.0);
        bikes[3] = new vehicles.bicycle("scott", 27, 56.0);

        for (int i = 0; i < bikes.length; i++) {
            System.out.println("Bike " + (i + 1) + ": " + bikes[i]);
        }

    }
}